// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import {IGames} from "../../src/pages/Games/config/interfaces";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<IGames[] | IGames | undefined>
) {
  const { game, select, sort, platform, search, page } = req.query as { [key: string]: any };
  if (search) {
    const games: IGames[] | undefined = [...GAMES, ...GAMES2].filter((el) => el.name.toLowerCase().includes(search.toLowerCase()));
    res.status(200).json(games);
    return;
  }
  if (game) {
    const games: IGames | undefined = [...GAMES, ...GAMES2].find((el) => el.path === game);
    res.status(200).json(games);
    return;
  }
  if (select || sort || platform || page) {
    let initialGames: IGames[] | undefined = GAMES;
    if (!!+page) {
      initialGames = [...GAMES, ...GAMES2];
    }
    const games = initialGames.filter((el) => {
      if (platform) {
       return el.released.includes(select) && el.platforms.includes(platform);
      }
      return el.released.includes(select);
    }).sort((a, b) => {
      if (sort === 'desc') {
        return a.rating - b.rating;
      }
      if (sort === 'asc') {
        return a.rating - b.rating;
      }
      return 0;
    });
    res.status(200).json(games);
    return;
  }
  res.status(200).json(GAMES);
}

export const GAMES = [
  {
    id: 1,
    name: 'Grand Theft Auto V',
    path: 'grand-theft-auto-v',
    background_image: 'https://cloudgamers.ru/wp-content/uploads/2021/09/gta5-scaled.jpg',
    rating: 91,
    released: '09-17-2013',
    platforms: ['playstation', 'xbox', 'pc'],
    about: 'Rockstar Games went bigger, since their previous installment of the series. You get the complicated and realistic world-building from Liberty City of GTA4 in the setting of lively and diverse Los Santos, from an old fan favorite GTA San Andreas. 561 different vehicles (including every transport you can operate) and the amount is rising with every update.\n' +
      'Simultaneous storytelling from three unique perspectives:\n' +
      'Follow Michael, ex-criminal living his life of leisure away from the past, Franklin, a kid that seeks the better future, and Trevor, the exact past Michael is trying to run away from.\n' +
      'GTA Online will provide a lot of additional challenge even for the experienced players, coming fresh from the story mode. Now you will have other players around that can help you just as likely as ruin your mission. Every GTA mechanic up to date can be experienced by players through the unique customizable character, and community content paired with the leveling system tends to keep everyone busy and engaged.'
  },
  {
    id: 2,
    name: 'The Witcher 3: Wild Hunt',
    path: 'the-witcher-3-wild-hunt',
    background_image: 'https://www.ixbt.com/img/n1/news/2022/4/4/FTIN491VEAAA7eQ_large.jpg',
    rating: 92,
    released: '05-18-2015',
    platforms: ['playstation', 'xbox', 'pc'],
    about: 'The third game in a series, it holds nothing back from the player. Open world adventures of the renowned monster slayer Geralt of Rivia are now even on a larger scale. Following the source material more accurately, this time Geralt is trying to find the child of the prophecy, Ciri while making a quick coin from various contracts on the side. Great attention to the world building above all creates an immersive story, where your decisions will shape the world around you.\n' +
      '\n' +
      'CD Project Red are infamous for the amount of work they put into their games, and it shows, because aside from classic third-person action RPG base game they provided 2 massive DLCs with unique questlines and 16 smaller DLCs, containing extra quests and items.\n' +
      '\n' +
      'Players praise the game for its atmosphere and a wide open world that finds the balance between fantasy elements and realistic and believable mechanics, and the game deserved numerous awards for every aspect of the game, from music to direction.'
  },
  {
    id: 3,
    name: 'Portal 2',
    path: 'portal-2',
    background_image: 'https://cdn.igromania.ru/mnt/articles/3/6/f/b/7/4/18700/html/img/7bd83786fd51ad58_zoom.jpg',
    rating: 92,
    released: '04-18-2011',
    platforms: ['playstation', 'xbox', 'pc'],
    about: 'Portal 2 is a first-person puzzle game developed by Valve Corporation and released on April 19, 2011 on Steam, PS3 and Xbox 360. It was published by Valve Corporation in digital form and by Electronic Arts in physical form.\n' +
      '\n' +
      'Its plot directly follows the first game\'s, taking place in the Half-Life universe. You play as Chell, a test subject in a research facility formerly ran by the company Aperture Science, but taken over by an evil AI that turned upon its creators, GladOS. After defeating GladOS at the end of the first game but failing to escape the facility, Chell is woken up from a stasis chamber by an AI personality core, Wheatley, as the unkempt complex is falling apart. As the two attempt to navigate through the ruins and escape, they stumble upon GladOS, and accidentally re-activate her...',
  },
  {
    id: 4,
    name: 'God of War',
    path: 'god-of-war',
    background_image: 'https://cdn1.epicgames.com/offer/3ddd6a590da64e3686042d108968a6b2/EGS_GodofWar_SantaMonicaStudio_S2_1200x1600-fbdf3cbc2980749091d52751ffabb7b7_1200x1600-fbdf3cbc2980749091d52751ffabb7b7',
    rating: 94,
    released: '04-20-2018',
    platforms: ['playstation', 'xbox'],
    about: 'It is a new beginning for Kratos. Living as a man outside the shadow of the gods, he ventures into the brutal Norse wilds with his son Atreus, fighting to fulfill a deeply personal quest.\n' +
      '\n' +
      'His vengeance against the Gods of Olympus years behind him, Kratos now lives as a man in the realm of Norse Gods and monsters. It is in this harsh, unforgiving world that he must fight to survive… And teach his son to do the same. This startling reimagining of God of War deconstructs the core elements that defined the series—satisfying combat; breathtaking scale; and a powerful narrative—and fuses them anew.\n' +
      '\n' +
      'Kratos is a father again. As mentor and protector to Atreus, a son determined to earn his respect, he is forced to deal with and control the rage that has long defined him while out in a very dangerous world with his son.',
  },
  {
    id: 5,
    name: 'Dota 2',
    path: 'dota-2',
    background_image: 'https://media.rawg.io/media/crop/600/400/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg',
    rating: 90,
    released: '7-09-2013',
    platforms: ['pc'],
    about: 'What used to be an unofficial modded map for the Warcraft 3, ended up being the most budgeted cybersport discipline, gathering millions of people to watch annual international championships.\n' +
      'MOBA genre started with the DOTA, Defense of the Ancients, which can be efficiently described as 5 vs 5 top-down action strategy game, during which players are tasked to destroy the enemy core while protecting their own.\n' +
      'Players can pick out of the roster of 112 heroes and battle on the single map while taking advantage of field vision, resources and item build that can either make heroes stronger or disable the enemy. DOTA 2 is still active, and receives updates weekly, reshaping metas and refreshing game balance, if by any chance some heroes became unreasonably strong. Each hero has not only a unique set of abilities but is fully customizable, through getting items for heroes after matches of through the trade. Not only heroes but terrain, couriers, that deliver items for you and even match announcers, that can be voiced by heroes, professional casters of just memorable characters from other forms of media.',
    screenshots: ['https://media.rawg.io/media/crop/600/400/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg', 'https://3dnews.ru/assets/external/illustrations/2020/05/05/1010145/DOTA-2.jpg', 'https://hyperpc.ru/images/product/lumen/dota-2-edition/main/hyperpc-lumen-dota-2-banner-alt-2.jpg', 'https://cybersport.metaratings.ru/storage/images/1d/4f/1d4fe8b607874e5d82a635fd14713fce.jpg', 'https://cdn.kanobu.ru/r/837e765d794764a1aa04a6512a7e5cf0/1040x-/cdn.kanobu.ru/editor/images/31/adb73791-b9ad-4b8f-8f1f-e938f41de009.jpg'],
  },
];

const GAMES2 = [
  {
    id: 6,
    name: 'Dota 3',
    path: 'dota-3',
    background_image: 'https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg',
    rating: 80,
    screenshots: ['https://media.rawg.io/media/crop/600/400/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg', 'https://3dnews.ru/assets/external/illustrations/2020/05/05/1010145/DOTA-2.jpg', 'https://hyperpc.ru/images/product/lumen/dota-2-edition/main/hyperpc-lumen-dota-2-banner-alt-2.jpg', 'https://cybersport.metaratings.ru/storage/images/1d/4f/1d4fe8b607874e5d82a635fd14713fce.jpg', 'https://cdn.kanobu.ru/r/837e765d794764a1aa04a6512a7e5cf0/1040x-/cdn.kanobu.ru/editor/images/31/adb73791-b9ad-4b8f-8f1f-e938f41de009.jpg'],
    released: '9-07-2013',
    platforms: ['pc'],
    about: 'What used to be an unofficial modded map for the Warcraft 3, ended up being the most budgeted cybersport discipline, gathering millions of people to watch annual international championships.\n' +
      'MOBA genre started with the DOTA, Defense of the Ancients, which can be efficiently described as 5 vs 5 top-down action strategy game, during which players are tasked to destroy the enemy core while protecting their own.\n' +
      'Players can pick out of the roster of 112 heroes and battle on the single map while taking advantage of field vision, resources and item build that can either make heroes stronger or disable the enemy. DOTA 2 is still active, and receives updates weekly, reshaping metas and refreshing game balance, if by any chance some heroes became unreasonably strong. Each hero has not only a unique set of abilities but is fully customizable, through getting items for heroes after matches of through the trade. Not only heroes but terrain, couriers, that deliver items for you and even match announcers, that can be voiced by heroes, professional casters of just memorable characters from other forms of media.',
  },
  {
    id: 7,
    name: 'Dota 4',
    path: 'dota-4',
    screenshots: ['https://media.rawg.io/media/crop/600/400/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg', 'https://3dnews.ru/assets/external/illustrations/2020/05/05/1010145/DOTA-2.jpg', 'https://hyperpc.ru/images/product/lumen/dota-2-edition/main/hyperpc-lumen-dota-2-banner-alt-2.jpg', 'https://cybersport.metaratings.ru/storage/images/1d/4f/1d4fe8b607874e5d82a635fd14713fce.jpg', 'https://cdn.kanobu.ru/r/837e765d794764a1aa04a6512a7e5cf0/1040x-/cdn.kanobu.ru/editor/images/31/adb73791-b9ad-4b8f-8f1f-e938f41de009.jpg'],

    background_image: 'https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg',
    rating: 94,
    released: '9-07-2013',
    platforms: ['pc'],
    about: 'What used to be an unofficial modded map for the Warcraft 3, ended up being the most budgeted cybersport discipline, gathering millions of people to watch annual international championships.\n' +
      'MOBA genre started with the DOTA, Defense of the Ancients, which can be efficiently described as 5 vs 5 top-down action strategy game, during which players are tasked to destroy the enemy core while protecting their own.\n' +
      'Players can pick out of the roster of 112 heroes and battle on the single map while taking advantage of field vision, resources and item build that can either make heroes stronger or disable the enemy. DOTA 2 is still active, and receives updates weekly, reshaping metas and refreshing game balance, if by any chance some heroes became unreasonably strong. Each hero has not only a unique set of abilities but is fully customizable, through getting items for heroes after matches of through the trade. Not only heroes but terrain, couriers, that deliver items for you and even match announcers, that can be voiced by heroes, professional casters of just memorable characters from other forms of media.',
  },
  {
    id: 8,
    name: 'Dota 5',
    path: 'dota-5',
    screenshots: ['https://media.rawg.io/media/crop/600/400/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg', 'https://3dnews.ru/assets/external/illustrations/2020/05/05/1010145/DOTA-2.jpg', 'https://hyperpc.ru/images/product/lumen/dota-2-edition/main/hyperpc-lumen-dota-2-banner-alt-2.jpg', 'https://cybersport.metaratings.ru/storage/images/1d/4f/1d4fe8b607874e5d82a635fd14713fce.jpg', 'https://cdn.kanobu.ru/r/837e765d794764a1aa04a6512a7e5cf0/1040x-/cdn.kanobu.ru/editor/images/31/adb73791-b9ad-4b8f-8f1f-e938f41de009.jpg'],

    background_image: 'https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg',
    rating: 82,
    released: '9-07-2013',
    platforms: ['pc'],
    about: 'What used to be an unofficial modded map for the Warcraft 3, ended up being the most budgeted cybersport discipline, gathering millions of people to watch annual international championships.\n' +
      'MOBA genre started with the DOTA, Defense of the Ancients, which can be efficiently described as 5 vs 5 top-down action strategy game, during which players are tasked to destroy the enemy core while protecting their own.\n' +
      'Players can pick out of the roster of 112 heroes and battle on the single map while taking advantage of field vision, resources and item build that can either make heroes stronger or disable the enemy. DOTA 2 is still active, and receives updates weekly, reshaping metas and refreshing game balance, if by any chance some heroes became unreasonably strong. Each hero has not only a unique set of abilities but is fully customizable, through getting items for heroes after matches of through the trade. Not only heroes but terrain, couriers, that deliver items for you and even match announcers, that can be voiced by heroes, professional casters of just memorable characters from other forms of media.',
  },
  {
    id: 9,
    name: 'Dota 6',
    path: 'dota-6',
    screenshots: ['https://media.rawg.io/media/crop/600/400/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg', 'https://3dnews.ru/assets/external/illustrations/2020/05/05/1010145/DOTA-2.jpg', 'https://hyperpc.ru/images/product/lumen/dota-2-edition/main/hyperpc-lumen-dota-2-banner-alt-2.jpg', 'https://cybersport.metaratings.ru/storage/images/1d/4f/1d4fe8b607874e5d82a635fd14713fce.jpg', 'https://cdn.kanobu.ru/r/837e765d794764a1aa04a6512a7e5cf0/1040x-/cdn.kanobu.ru/editor/images/31/adb73791-b9ad-4b8f-8f1f-e938f41de009.jpg'],

    background_image: 'https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg',
    rating: 70,
    released: '9-07-2013',
    platforms: ['pc'],
    about: 'What used to be an unofficial modded map for the Warcraft 3, ended up being the most budgeted cybersport discipline, gathering millions of people to watch annual international championships.\n' +
      'MOBA genre started with the DOTA, Defense of the Ancients, which can be efficiently described as 5 vs 5 top-down action strategy game, during which players are tasked to destroy the enemy core while protecting their own.\n' +
      'Players can pick out of the roster of 112 heroes and battle on the single map while taking advantage of field vision, resources and item build that can either make heroes stronger or disable the enemy. DOTA 2 is still active, and receives updates weekly, reshaping metas and refreshing game balance, if by any chance some heroes became unreasonably strong. Each hero has not only a unique set of abilities but is fully customizable, through getting items for heroes after matches of through the trade. Not only heroes but terrain, couriers, that deliver items for you and even match announcers, that can be voiced by heroes, professional casters of just memorable characters from other forms of media.',
  },
  {
    id: 10,
    name: 'Dota 7',
    path: 'dota-7',
    background_image: 'https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg',
    rating: 20,
    released: '9-07-2013',
    platforms: ['pc'],
    about: 'What used to be an unofficial modded map for the Warcraft 3, ended up being the most budgeted cybersport discipline, gathering millions of people to watch annual international championships.\n' +
      'MOBA genre started with the DOTA, Defense of the Ancients, which can be efficiently described as 5 vs 5 top-down action strategy game, during which players are tasked to destroy the enemy core while protecting their own.\n' +
      'Players can pick out of the roster of 112 heroes and battle on the single map while taking advantage of field vision, resources and item build that can either make heroes stronger or disable the enemy. DOTA 2 is still active, and receives updates weekly, reshaping metas and refreshing game balance, if by any chance some heroes became unreasonably strong. Each hero has not only a unique set of abilities but is fully customizable, through getting items for heroes after matches of through the trade. Not only heroes but terrain, couriers, that deliver items for you and even match announcers, that can be voiced by heroes, professional casters of just memorable characters from other forms of media.',
  },
];
