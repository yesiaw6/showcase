import React from 'react';
import {NextPage} from "next";
import {IGamesProps} from "../../src/pages/Games/config/interfaces";
import {Games} from "../../src/pages";

export async function getServerSideProps() {
  const results = await fetch('http://localhost:3000/api/games').then(res => res.json());
  return {
    props: {
      results,
    },
  }
}

const GamesPage: NextPage<IGamesProps> = ({ results }) => {
  return (
    <Games results={results} />
  );
};

export default GamesPage;
