import React from 'react';
import {NextPage} from "next";
import {BigCard} from "../../src/entities/GameCard";
import {IGameProps, IParams} from "../../src/pages/Games/config/interfaces";

export async function getServerSideProps (context: IParams) {
  const { game } = context.params;
  const result = await fetch('http://localhost:3000/api/games?' + new URLSearchParams({ game })).then(res => res.json());
  return {
    props: {
      result
    },
  }
}

const Game: NextPage<IGameProps> = ({ result }) => {
  return (
    <BigCard
      {...result}
    />
  );
};

export default Game;
