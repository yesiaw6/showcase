import {api} from "../../../../api";

export default (search: string, setGames: (res: never[]) => void, setLoading: (value: boolean) => void) => {
  const queryParams = { params: { search } }
  const results = api.getGames(queryParams).then(res => {
    setLoading(false);
    setGames(res);
  });
}