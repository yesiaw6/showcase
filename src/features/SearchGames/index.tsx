import React, {useEffect, useState} from 'react';
import {OutlinedInput} from "../../shared/ui";
import {getGamesBySearch} from "./model";
import {Container} from "./styles";
import {useDebounce} from "../../shared/helpers";
import {useRouter} from "next/router";
import {GameList} from "./ui";

const SearchGames = () => {
  const router = useRouter();
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');

  const debounce = useDebounce((data: string) => getGamesBySearch(data, setGames, setLoading));

  const onSearchChange = (e: any) => {
    setLoading(true);
    setSearch(e);
  };

  const onSelectGame = (path: string) => {
    router.push(`/games/${path}`);
    setSearch('');
  };

 /* TODO better use area mouse click listener */
  const onBlurSearch = () => {
    setTimeout(() => {
      setSearch('');
    }, 300);
  };

  useEffect(() => {
    debounce(search);
  }, [search]);
  return (
    <Container>
      <OutlinedInput
        placeholder="Search games..."
        value={search}
        onChange={onSearchChange}
        onBlur={onBlurSearch}
      />
      {search && (
        <GameList loading={loading} games={games} onSelectGame={onSelectGame} />
      )}
    </Container>
  );
};

export default SearchGames;

