import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100%;
  position: relative;
`;
export const ListContainer = styled.div`
  position: absolute;
  top: 60px;
  z-index: 10;
  display: flex;
  flex-direction: column;
  gap: 10px;
  height: max-content;
  max-height: 500px;
  width: 100%;
  border-radius: 18.5px;
  padding: 16px;
  background-color: #000;
  overflow-y: scroll;
`;
export const GamesList = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;