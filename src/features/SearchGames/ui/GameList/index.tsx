import React, {FC} from 'react';
import {Loader} from "../../../../shared/ui";
import {GamesList, ListContainer} from "../../styles";
import {IGames} from "../../../../pages/Games/config/interfaces";
import {SmallCard} from "../../../../entities/GameCard";
import {IGameList} from "../../config/interfaces";

const GameList: FC<IGameList> = ({ loading, games, onSelectGame }) => {
  return (
    <ListContainer>
      {loading ? (
        <Loader />
      ) : (
        <div>
          {games.length ? (
            <GamesList>
              {games.map((game: IGames) => (
                <SmallCard
                  key={game.id}
                  {...game}
                  onSelect={onSelectGame}
                />
              ))}
            </GamesList>
          ) : (
            <p>
              Nothing found...
            </p>
          )}
        </div>
      )}
    </ListContainer>
  );
};

export default GameList;

