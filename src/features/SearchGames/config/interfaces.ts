import {IGames} from "../../../pages/Games/config/interfaces";

export interface IGameList {
  loading: boolean;
  games: IGames[];
  onSelectGame: (path: string) => void;
}