export const api = {
  getGames: async ({ params }: { params?: object }) => {
    return await fetch('http://localhost:3000/api/games?' + new URLSearchParams({ ...params }))
      .then(res => res.json());
  },
};