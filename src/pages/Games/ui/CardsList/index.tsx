import React, {FC} from 'react';

import {MediumCard} from "../../../../entities/GameCard";
import {CardsContainer} from "../../styles";
import {IGamesProps} from "../../config/interfaces";

const CardsList: FC<IGamesProps> = ({ results }) => {
  return (
    <CardsContainer>
      {results.map((game) => (
        <MediumCard
          key={game?.id}
          rating={game?.rating}
          released={game?.released}
          platforms={game?.platforms}
          background_image={game?.background_image}
          name={game?.name}
          path={game?.path}
        />
      ))}
    </CardsContainer>
  );
};

export default CardsList;
