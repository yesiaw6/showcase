import {IGetSelectGames} from "../../config/interfaces";
import {api} from "../../../../api";

export const getSelectGames = ({ select, sort, platform, setResults, page }: IGetSelectGames) => {
  const queryParams = { params: { select, sort, platform, page } }
  api.getGames(queryParams).then(res => setResults(res));
};
