import {IOptions} from "../../../shared/config/interfaces";

export const RELEASED_SELECT_OPTIONS: IOptions[] = [
  { label: '2011', value: '2011' },
  { label: '2013', value: '2013' },
  { label: '2015', value: '2015' },
  { label: '2018', value: '2018' },
];
export const RATING_SORTED_OPTIONS: IOptions[] = [
  { label: 'Sort Descending', value: 'desc' },
  { label: 'Sort Ascending', value: 'asc' },
];
export const PLATFORM_OPTIONS: IOptions[] = [
  { label: 'Playstation', value: 'playstation' },
  { label: 'Xbox', value: 'xbox' },
  { label: 'Pc', value: 'pc' },
];