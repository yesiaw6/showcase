import {Dispatch, SetStateAction} from "react";

export interface IGames {
  id: number;
  name: string;
  path: string;
  background_image: string;
  rating: number;
  released: string;
  platforms: string[];
  about: string;
  screenshots?: string[];
}

export interface IGamesProps {
  results: IGames[];
}
export interface IGameProps {
  result: IGames;
}
export interface IParams {
  params: {
    game: string;
  };
}

export interface IGetSelectGames {
  select: string;
  sort: string;
  platform: string;
  page: number;
  setResults: Dispatch<SetStateAction<IGames[]>>;
}