import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  width: 100%;
`;
export const CardsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 25px;
`;
export const ActionsContainer = styled.div`
  width: max-content;
  display: flex;
  gap: 10px;
  @media (max-width: 587px) {
    flex-direction: column;
  }
`;