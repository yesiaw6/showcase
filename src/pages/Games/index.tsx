import React, {FC, useEffect, useMemo, useState} from 'react';
import {ActionsContainer, Container, CardsContainer} from "./styles";
import {DinamycScroll, Select} from "../../shared/ui";
import {PLATFORM_OPTIONS, RATING_SORTED_OPTIONS, RELEASED_SELECT_OPTIONS} from "./config";
import {IGamesProps} from "./config/interfaces";
import {CardsList} from "./ui";
import {getSelectGames} from "./model";

/* TODO refactor all this on Context orr add redux */

const Games: FC<IGamesProps> = ({ results: initialResults }) => {
  const [results, setResults] = useState(initialResults);
  const [page, setPage] = useState(0);
  const [select, setSelect] = useState('');
  const [sort, setSort] = useState('');
  const [platform, setPlatform] = useState('');

  useEffect(() => {
    getSelectGames({ select, setResults, sort, platform, page });
  }, [select, sort, platform, page]);

  const MemoizedCardList = useMemo(() => <CardsList results={results} />, [results]);

  return (
    <Container>
      <ActionsContainer>
        <Select
          placeholder="Released date"
          options={RELEASED_SELECT_OPTIONS}
          onChange={setSelect}
          value={select}
        />
        {select && (
          <button
            onClick={() => setSelect('')}
          >
            Clear
          </button>
        )}
        <Select
          placeholder="Sorting"
          options={RATING_SORTED_OPTIONS}
          onChange={setSort}
          value={sort}
        />
        {sort && (
          <button
            onClick={() => setSort('')}
          >
            Clear
          </button>
        )}
        <Select
        placeholder="Platform"
        options={PLATFORM_OPTIONS}
        onChange={setPlatform}
        value={platform}
        />
        {platform && (
          <button
            onClick={() => setPlatform('')}
          >
            Clear
          </button>
        )}
      </ActionsContainer>
      <CardsContainer>
        {MemoizedCardList}
      </CardsContainer>
      <DinamycScroll
        callback={() => setPage(prev => prev + 1)}
        loading={true}
        isNextPage={!page}
      />
    </Container>
  );
};

export default Games;

