import {Dispatch, SetStateAction} from "react";

export interface IControlledInput {
  value?: string;
  onChange?: Dispatch<SetStateAction<string>>;
}

export interface IOutlinedInput extends IControlledInput {
  placeholder?: string;
  onBlur?: () => void ;
}
export interface IOptions {
  value: string;
  label: string;
}
export interface ISelectInput extends IControlledInput {
  placeholder?: string;
  options?: IOptions[];
}

export interface IDinamycScroll {
  callback: () => void;
  loading: boolean;
  isNextPage: boolean;
}

export interface ISlider {
  photos: string[];
}