import styled from "styled-components";

export const Input = styled.input`
  width: 100%;
  flex-grow: 1;
  transition: all .3s linear;
  height: 44px;
  border-radius: 24px;
  background-color: hsla(0,0%,100%,.16);
  padding: 20px;
  border: none;
  outline: none;
  
  &:hover, &:focus{
    color: #000;
    background-color: #fff;
  }
`;