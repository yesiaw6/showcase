import React, {FC} from 'react';
import {Input} from './styles';
import {IOutlinedInput} from "../../config/interfaces";

const OutlinedInput: FC<IOutlinedInput> = ({ value, onChange, placeholder, onBlur }) => {
  return (
    <Input
      onChange={(e) => onChange ? onChange(e.target.value) : () => {}}
      value={value}
      placeholder={placeholder}
      onBlur={onBlur}
    />
  );
};

export default OutlinedInput;

