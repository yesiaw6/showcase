import OutlinedInput from "./OutlinedInput";
import Select from "./Select";
import Loader from "./Loader";
import DinamycScroll from "./DinamycScroll";
import Slider from "./Slider";

export { OutlinedInput, Select, Loader, DinamycScroll, Slider };
