import styled, { keyframes }  from "styled-components";

const lds_dual_ring = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`

export const LoaderContainer = styled.div`
  display: flex;
  width: 80px;
  height: 80px;
  
  &::after{
    content: " ";
    display: block;
    width: 64px;
    height: 64px;
    border-radius: 50%;
    border: 6px solid #fff;
    border-color: #fff transparent #fff transparent;
    animation-name: ${lds_dual_ring};
    animation-duration: 1.2s;
    animation-iteration-count: infinite;
  }
`;
