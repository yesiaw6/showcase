import React from 'react';
import { LoaderContainer } from './styles';

const Loader = () => {
  return (
    <LoaderContainer />
  );
};

export default Loader;
