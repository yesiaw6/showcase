import React, {FC} from 'react';
import {ISelectInput} from "../../config/interfaces";
import {SelectTag, OptionTag} from "./styles";

const Select: FC<ISelectInput> = ({ value: currentValue, onChange, options = [], placeholder }) => {
  return (
    <SelectTag
      onChange={(e) => onChange ? onChange(e.target.value) : null}
    >
      <option disabled selected={'' === currentValue} hidden>{placeholder}</option>
      {options.map(({ label, value }) => (
        <OptionTag
          key={label}
          value={value}
          selected={value === currentValue}
        >
          {label}
        </OptionTag>
      ))}
    </SelectTag>
  );
};

export default Select;
