import React, {FC, useEffect} from 'react';

import Loader from '../Loader';
import {IDinamycScroll} from "../../config/interfaces";
import {useInView} from "../../helpers";

const DinamycScroll: FC<IDinamycScroll> = ({ callback, loading, isNextPage }) => {
  const { ref, inView } = useInView({
    threshold: 0.5,
  });
  useEffect(() => {
    if (inView) {
      callback();
    }
  }, [inView]);
  if (!isNextPage) {
    return null;
  }
  return (
    <div ref={ref}>
      {loading && <Loader />}
    </div>
  );
};

export default DinamycScroll;
