import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  position: relative;
  width: 100%;
`;

export const PhotoContainer = styled.div<{ index: number }>`
  transform: ${props => `translateX(-${props.index * 100}%)`};
  position: absolute;
  display: flex;
  width: 100%;
  height: 100%;
  transition: all 1s;
`;
export const Photo = styled.img`
  display: flex;
`;