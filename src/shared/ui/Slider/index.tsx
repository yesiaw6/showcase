import React, {FC, useEffect, useState} from 'react';
import {ISlider} from "../../config/interfaces";
import {PhotoContainer, Photo, Container} from './styles';

const Slider: FC<ISlider> = ({ photos }) => {
  const [slide, setSlide] = useState(0);
  console.log(slide)

  const nextSlide = () => {

    setSlide(prev => {
      if (prev === photos.length - 1) {
        return 0 ;
      }
      return prev + 1;
    });
  };

  useEffect(() => {
    const interval = setInterval(nextSlide, 3000);

    return () => {
      clearTimeout(interval);
    };
  }, []);

  return (
    <Container>
      <PhotoContainer index={slide}>
        {photos.map((photo) => (
          <Photo src={photo} alt="photo" />
        ))}
      </PhotoContainer>
    </Container>
  );
};

export default Slider;
