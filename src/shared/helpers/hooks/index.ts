import {MutableRefObject, useEffect, useRef, useState} from "react";

export const useDebounce = (callback: (arg: any) => void, ms: number = 300) => {
  const timer: MutableRefObject<string | NodeJS.Timeout | undefined> = useRef();

  return (e: any) => {
    if (timer.current) {
      clearTimeout(timer.current);
    }
    timer.current = setTimeout(() => {
      callback(e);
    }, ms);
  };
};

export const useInView = ({ threshold }: { threshold: number }) => {
  const ref: any = useRef();
  const observer: any = useRef();
  const [inView, setInView] = useState(false);
  const options = {
    root: ref.current,
    threshold,
  }
  const callback = function(entries: any) {
    setInView(entries[0]?.isIntersecting);
  };

  useEffect(() => {
    if (ref.current) {
      observer.current = new IntersectionObserver(callback, options);
      observer.current.observe(ref.current);
    }
  }, [ref.current]);

  return { ref, inView };
};