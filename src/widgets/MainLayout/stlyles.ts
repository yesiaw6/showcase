import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const HeaderContainer = styled.div`
  display: flex;
  width: 100%;
  padding: 24px 40px;
`;
export const MenuContainer = styled.div`
  display: flex;
  padding: 40px;
`;
export const ContentContainer = styled.div`
  display: flex;
  flex-grow: 1;
  padding: 40px;
  min-width: 1%;
  width: 100%;
`;