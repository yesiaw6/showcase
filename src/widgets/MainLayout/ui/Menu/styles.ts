import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  flex-shrink: 0;
  @media (max-width: 768px) {
    flex-direction: row;
  }
`;

export const LinkItem = styled.p`
  font-size: 24px;
  text-decoration: none;
  cursor: pointer;
  transition: all .2s ease-out;
  
  &:hover{
    color: hsla(0,0%,100%,.4);
  }
`;