import React, {FC} from 'react';
import Link from "next/link";
import {Container, LinkItem} from './styles';
import {IMenuItem} from "../../config/interfaces";
import {MENU} from "../../config/config";

const Menu: FC = () => {
  return (
    <Container>
      {MENU.map(({name, path}:IMenuItem) => (
        <Link
          href={path}
          key={path}
        >
          <LinkItem>
            {name}
          </LinkItem>
        </Link>
      ))}
    </Container>
  );
};

export default Menu;

