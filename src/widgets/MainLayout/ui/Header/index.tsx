import React, {FC} from 'react';
import {Title} from './styles';
import {Container} from './styles';
import SearchGames from "../../../../features/SearchGames";

const Header: FC = () => {
  return (
    <Container>
      <Title>
        game Showcase
      </Title>
      <SearchGames />
    </Container>
  );
};

export default Header;

