import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  gap: 20px;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Title = styled.div`
  font-size: 24px;
  text-transform: uppercase;
  letter-spacing: 5px;
`;