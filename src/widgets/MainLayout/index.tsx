import React, {FC} from 'react';
import {ILayout} from "./config/interfaces";
import {Container, ContentContainer, HeaderContainer, MenuContainer} from "./stlyles";
import {Header, Menu} from "./ui";

const MainLayout: FC<ILayout> = ({ children }) => {
  return (
    <>
      <HeaderContainer>
        <Header />
      </HeaderContainer>
      <Container>
        <MenuContainer>
          <Menu />
        </MenuContainer>
        <ContentContainer>
          {children}
        </ContentContainer>
      </Container>
    </>
  );
};

export default MainLayout;
