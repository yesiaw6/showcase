import React from "react";

export interface ILayout {
  children: string | React.ReactNode | null;
}
export interface IMenuItem {
  name: string;
  path: string;
}