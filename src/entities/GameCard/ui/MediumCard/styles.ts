import styled from "styled-components";

export const Wrapper = styled.div<{ isOpen: boolean }>`
  position: relative;
  width: calc(100% / 3 - 25px);
  flex-grow: 1;
  gap: 5px;
  min-width: 250px;
  max-width: 400px;
`;
export const Container = styled.div<{ isOpen: boolean }>`
  width: 100%;
  //position: ${props => props.isOpen ? 'absolute' : 'relative'};
  z-index: ${props => props.isOpen ? '5' : '1'};
  display: flex;
  flex-direction: column;
  background-color: #202020;
  border-radius: 12px;
  box-shadow: 0 10px 20px 0 rgb(0 0 0 / 7%);
  transition: all .3s;
  transform: ${props => props.isOpen ? 'scale(1.02)' : 'scale(1)'};
  overflow: hidden;
`;
export const PhotoContainer = styled.div`
  display: flex;
  position: relative;
  padding-bottom: 60%;
`;
export const Photo = styled.div<{ backgroundImage: string }>`
  position: absolute;
  background-image: url(${props => props.backgroundImage});
  background-position-y: 15%;
  background-size: cover;
  background-repeat: no-repeat;
  width: 100%;
  height: 100%;
`;
export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 16px 16px 24px;
`;
export const Rating = styled.p`
  width: max-content;
  padding: 2px 5px;
  color: #6dc849;
  font-weight: 700;
  text-align: center;
  border-radius: 4px;
  border: 1px solid rgba(109,200,73,.4);
`;
export const Title = styled.p`
  font-size: 24px;
  font-weight: 700;
  text-decoration: none;
  cursor: pointer;
  transition: all .2s ease-out;

  &:hover{
    color: hsla(0,0%,100%,.4);
  }
`;
export const FooterContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
export const FooterItem = styled.div`
  display: flex;
  align-items: center;
  
  &:not(:last-child){
    padding-bottom: 12px;
    border-bottom: 1px solid;
    color: hsla(0, 0%, 100%, .07);
  }
`;
export const ItemTitle = styled.p`
  flex-grow: 1;
  font-size: 12px;
  color: hsla(0, 0%, 100%, .4);
`;
export const ItemText = styled.p`
  font-size: 12px;
  font-weight: 400;
  color: white;
`;