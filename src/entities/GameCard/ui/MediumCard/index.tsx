import React, {FC, useState} from 'react';
import {
  Wrapper,
  Photo,
  ContentContainer,
  PhotoContainer,
  Rating,
  Title,
  FooterContainer,
  FooterItem,
  ItemTitle,
  ItemText,
  Container,
} from "./styles";
import {IMediumCard} from "../../config/interfaces";
import Link from "next/link";
import {dateTimeFormat, toCamel} from "../../lib";

const MediumCard: FC<IMediumCard> = ({ background_image, name, rating, path, released, platforms }) => {
  const [isOpen, setIsOpen] = useState(false);
  const IntlReleased = dateTimeFormat(new Date(released));
  return (
    <Wrapper
      isOpen={isOpen}
      onMouseOver={() => setIsOpen(true)}
      onMouseLeave={() => setIsOpen(false)}
    >
      <Container
        isOpen={isOpen}
      >
        <PhotoContainer>
          <Photo backgroundImage={background_image} />
        </PhotoContainer>
        <ContentContainer>
          <Rating>
            {rating}
          </Rating>
          <Link href={`/games/${path}`}>
            <Title>
              {name}
            </Title>
          </Link>
            <FooterContainer>
              <FooterItem>
                <ItemTitle>
                  Release date:
                </ItemTitle>
                <ItemText>
                  {IntlReleased}
                </ItemText>
              </FooterItem>
              <FooterItem>
                <ItemTitle>
                  Platforms:
                </ItemTitle>
                {platforms.map((platform, index) => (
                  <ItemText
                    key={platform}
                  >
                    {toCamel(platform)}
                    {index === platforms.length - 1 ? '' : ','}
                    &nbsp;
                  </ItemText>
                ))}
              </FooterItem>
            </FooterContainer>
        </ContentContainer>
      </Container>
    </Wrapper>
  );
};

export default MediumCard;

