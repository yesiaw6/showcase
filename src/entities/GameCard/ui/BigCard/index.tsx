import React, {FC} from 'react';
import {Container, LeftBlock, Title, AboutContainer, AboutTitle, AboutText, RightBlock, PhotoContainer} from "./styles";
import {IBigCard} from "../../config/interfaces";
import {Slider} from "../../../../shared/ui";

const BigCard: FC<IBigCard> = ({ name, about, background_image, screenshots }) => {
  return (
    <Container>
      <LeftBlock>
        <Title>
          {name}
        </Title>
        <AboutContainer>
          <AboutTitle>
            About
          </AboutTitle>
          <AboutText>
            {about}
          </AboutText>
        </AboutContainer>
      </LeftBlock>
      <RightBlock>
        <PhotoContainer>
          {screenshots ? (
            <Slider photos={screenshots} />
          ) : (
            <div>
              <img src={background_image} alt="game_photo" />
            </div>
          )}
        </PhotoContainer>
      </RightBlock>
    </Container>
  );
};

export default BigCard;

