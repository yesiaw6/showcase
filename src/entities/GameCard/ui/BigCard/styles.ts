import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  gap: 50px;
  max-width: 960px;
  margin: auto;
  @media (max-width: 1100px) {
    flex-direction: column;
  }
`;
export const LeftBlock = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
  flex-grow: 1;
`;
export const Title = styled.div`
  font-size: 72px;
  font-weight: 700;
`;
export const AboutContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
export const AboutTitle = styled.div`
  font-size: 24px;
  font-weight: 700;
`;
export const AboutText = styled.div`
  font-size: 16px;
`;
export const RightBlock = styled.div`
  width: 400px;
  display: flex;
  flex-direction: column;
  gap: 15px;
  flex-shrink: 0;
`;
export const PhotoContainer = styled.div`
  max-width: 400px;
  height: 400px;
  width: 100%;
  display: flex;
  border-radius: 20px;
  overflow: hidden;
`;