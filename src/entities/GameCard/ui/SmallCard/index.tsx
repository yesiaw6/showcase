import React, {FC} from 'react';
import {Container, PhotoContainer, Photo, Title} from "./styles";
import {ISmallCard} from "../../config/interfaces";

const SmallCard:FC<ISmallCard> = ({ name, path, background_image, onSelect }) => {
  return (
    <Container
      role="button"
      tabIndex={0}
      onClick={() => onSelect(path)}
    >
      <PhotoContainer>
        <Photo backgroundImage={background_image} />
      </PhotoContainer>
      <Title>
        {name}
      </Title>
    </Container>
  );
};

export default SmallCard;
