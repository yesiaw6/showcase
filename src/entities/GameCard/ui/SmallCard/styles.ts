import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  cursor: pointer;
`;
export const PhotoContainer = styled.div`
  display: flex;
  width: 40px;
  height: 60px;
`;
export const Photo = styled.div<{ backgroundImage: string }>`
  background-image: url(${props => props.backgroundImage});
  background-position: 50%;
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 6px;
  cursor: pointer;
  width: 100%;
  height: 100%;
`;
export const Title = styled.p`
  font-size: 14px;
  font-weight: 700;
  transition: all .2s ease-out;
  cursor: pointer;
  &:hover{
    color: hsla(0,0%,100%,.4);
  }
`;