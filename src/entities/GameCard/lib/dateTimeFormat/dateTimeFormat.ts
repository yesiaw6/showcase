const defaultOptions = {
  year: "numeric",
  month: "short",
  day: "numeric",
};
export default (date: Date, options: object = defaultOptions) => {
  const formatter = new Intl.DateTimeFormat("en", options);

  return formatter.format(date);
};