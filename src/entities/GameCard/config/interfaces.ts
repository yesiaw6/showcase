export interface ISmallCard {
  background_image: string;
  name: string;
  path: string;
  onSelect?: (value: string) => void;
}

export interface IMediumCard extends ISmallCard {
  rating: number;
  released: string;
  platforms: string[];
}

export interface IBigCard extends IMediumCard {
  about: string;
  screenshots?: string[];
}